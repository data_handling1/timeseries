# timeseries

The aim of the project is to analyze timeseries data provided by
ZAMG. Therefore, anomalies can be calculated, plotted and stored.
Additionally, number of days exceeding 30° or fall below 0 can
be calculated and plotted.

It provides the following shell script: `timeseries`.
For usage information, type `timeseries --help`.


## Prerequisites

You need a working Python environment, and `pip` installed.


## Installation
1. Create virtual environment with:

```bash
python3 -m venv env.
```

2. Activate the virtual environment with:

```bash
source env/bin/activate.
```

3. For some reason one has to install wheel in venv via:

```bash
pip install wheel .
```

4. Use the following command in the base directory to install:

```bash
python -m pip install .
```

For an editable ("developer mode") installation, use the following
instead:

```bash
python -m pip install -e .
```

## How to run it

In order to run it an example is given below:

```bash
timeseries -csv <filename> -anom_plot -plot_var <t/tmax/tmin> -ext_plot -extremes <tmax/tmin> -plot_dir <directory path>

```

