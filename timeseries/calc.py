import numpy as np
import pandas as pd
import datetime

def calc_extreme_days(df,var):
    """ Counts number of hot or frost days.

    Parameters
    ----------
    df : pandas dataframe
      contains tmax and/or tmin.
    var : string
      selection of tmax or tmin.

    Returns
    -------
    pandas dataarray
      contains number of hot or frost days.
    """

    if var == "tmax":
        df = df[df[var]>30]

    elif var == "tmin":
        df = df[df[var]<0]

    da = df.groupby(df.index.year).size().dropna()
    da = da.rename(f"{df[var].name}_count")

    print("Extremes are calculated.")

    return da


def calc_anomalies(df):
    """Calculates monthly temperature anomalies.

    Parameters
    ----------
    df : pandas dataframe
      contains datetime object and temperature.

    Returns
    -------
    pandas dataframe
      contains temperature anomalies.
    """

    df_clim = df["1991-01":"2021-12"]
    clim = df_clim.groupby(df_clim.index.month).mean()
    clim.index.name = "month"

    monthly_means = df.groupby([df.index.year, df.index.month]).mean()
    monthly_means.index.names = ["year","month"]

    df_anom = monthly_means - clim

    # reset index to datetime index
    years = df_anom.index.get_level_values(0).astype(str)
    months = df_anom.index.get_level_values(1).astype(str)
    dates = pd.to_datetime(years + "-" + months + "-01")
    df_anom = df_anom.set_index(dates)

    print("Anomalies are calculated.")

    return df_anom


def calc_linear_fit(df,dependent_var,independent_var="time"):
    """Calculates trend of timeseries.

    Parameters
    ----------
    df : pandas dataframe
    dependent_var : string
      dependent variable used in linear fit.
    independent_var : string
      independent variable used in linear fit. The default is "time".

    Returns
    -------
    pandas dataframe
       contains trend column
    """

    # add time variable as column
    df["time"]=df.index

    # change dtype of datetime object to integer for linfit
    df["time"]=df['time'].map(datetime.datetime.toordinal)

    x = df[independent_var]
    y = df[dependent_var]


    fit = np.polyfit(x, y, 1)
    fit_fn = np.poly1d(fit)
    df["trend"] = fit_fn(x)

    df_anom_trend = df[[dependent_var,"trend"]].copy()

    print("Linear fit is computed.")

    return df_anom_trend
