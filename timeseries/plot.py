import matplotlib.pyplot as plt


def plot_extremes(da,plot_dir=""):
    """Plots timeseries of extreme days."""

    da.plot(grid=True)
    plt.ylabel("days")

    if da.name == "tmax_count":
         plt.title("Number of days with Tmax higher than 30°C")
    elif da.name == "tmin_count":
        plt.title("Number of days with Tmin lower than 0° ")

    plt.legend()
    plt.tight_layout()

    if plot_dir:
         plt.savefig(f"{plot_dir} timeseries_anomaly_{da.name}.png",dpi=300)
         plt.close()
         print("Plot of extreme days is created and saved to " + plot_dir)

    else:
        plt.show()


def plot_anomalies(df,var_name,plot_dir=""):
    """Plots monthly temperature anomalies."""

    plt.plot(df.index,df[var_name],"-k", label= df[var_name].name + " anomalies")

    if "trend" in df.columns:
         plt.plot(df.index,df["trend"] , '-r', label="linear fit",
                  linewidth = 3)

         plt.xlabel("time")
    plt.ylabel("temperature [°C]")
    plt.title(f"Monthly anomalies of {df[var_name].name}")
    plt.grid()
    plt.legend()
    plt.tight_layout()

    if plot_dir:
        plt.savefig(f"{plot_dir}/timeseries_anomaly_{df[var_name].name}.png",dpi=300)
        plt.close()
        print("Anomaly plot is created and saved to " + plot_dir)
    else:
       plt.show()
