import pandas as pd

def csv_to_df(fn):
    """Read csv-file and convert it to pandas dataframe."""

    df = pd.read_csv(fn,parse_dates=["time"],index_col="time")

    return df
