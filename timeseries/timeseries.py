import argparse
from pathlib import Path

from . import calc
from . import read
from . import plot #why does relative import with . not work

def timeseries():

        parser = argparse.ArgumentParser(
                """Program to calculate and plot timeseries data with or without trend
                as well as  days where temperatures exceed 30° or fall below 0°.""")
        parser.add_argument(
                "-csv",
                "--csv-data-file",
                dest="csv_data_file",
                required=False,
                help="Filename of input data in CSV format.",
        )
        parser.add_argument(
                "-anom_plot",
                "--anomalies_plot",
                dest="anomalies_plot",
                action="store_true",
                help="Plot anomalies.",
        )
        parser.add_argument(
                "-linfit",
                "--linear_fit",
                dest="linear_fit",
                action="store_true",
                help="Calculate linear fit of the anomalies.",
        )
        parser.add_argument(
                "-ind_var",
                "--independent_variable",
                dest="independent_variable",
                default="time",
                type=str,
                help="Independent variable for linear fit.",
        )
        parser.add_argument(
                "-plot_var",
                "--plot_variable",
                choices=["t","tmin","tmax"],
                dest="plot_var",
                type=str,
                help="""Variable to use for anomaly plot and
                        as dependent variable for linfit""",
        )
        parser.add_argument(
                "-ext_plot",
                "--extreme_plot",
                dest="extreme_plot",
                action="store_true",
                help="Plot days with extreme temperatures.",
        )
        parser.add_argument(
                "-extremes",
                "--extremes",
                choices=["tmax","tmin"],
                dest="extremes",
                required=False,
                help="Variable on which extreme is calculated.",
        )
        parser.add_argument(
                "--plot_dir",
                dest="plot_dir",
                required=False,
                type=str,
                help="""Directory in which plots are stored. If not given,
                figure is stored in already defined directory."""
        )

        args = parser.parse_args()

        if args.csv_data_file and not Path(args.csv_data_file).exists():
                parser.error(
                        f"File {args.csv_data_file} does not exist."
                )
        if args.linear_fit and (args.plot_var is None):
                parser.error(
                        "To calculate linear fit plot_variable has to be passed."
                )
        if args.anomalies_plot and (args.plot_var is None):
                parser.error(
                        "To plot timeseries variable to plot has to be given."
                )
        if args.extremes and (args.extremes is None):
                parser.error(
                        "To calculate extremes, a variable for computation is needed."
                )
        if args.extreme_plot and (args.extremes is None):
                parser.error(
                        "To plot extremes, first extremes have to be calculated"
                )


        if args.anomalies_plot:
                data = read.csv_to_df(args.csv_data_file)
                data = calc.calc_anomalies(data)

                if args.linear_fit:
                        data = calc.calc_linear_fit(
                                data,args.plot_var,args.independent_variable)

                plot.plot_anomalies(data,args.plot_var,args.plot_dir)

        if args.extreme_plot:
                #probably not the most elegant way to load read in same data twice
                #reset data so both commands can be used simultaneously
                data = read.csv_to_df(args.csv_data_file)
                data = calc.calc_extreme_days(data,args.extremes)
                data = plot.plot_extremes(data,args.plot_dir)
